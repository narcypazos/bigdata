import React, { Component } from 'react'
// react plugin for creating charts
import ChartistGraph from "react-chartist";
import { default as NumberFormat } from 'react-number-format';

// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

const useStyles = makeStyles(styles);
const allData = [];

class Dashboard extends Component {

  state = {
        allData: [],
        resume : {
        }, 
        guate: [],
        salvador: [],
        honduras: [],
        nicaragua: [],
        costa: [],
        pana: []

  
  }
  componentDidMount() {

    fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=')
          .then(res => res.json())
          .then((data) => {
          this.setState({ allData: data })
          })
          .catch(console.log)

    fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=GT')
          .then(res => res.json())
          .then((data) => {
            var series = [];
            var labels = [];
            var totales = [];

            Object.keys(data[0].timeseries).map(function(key, item){
                if (data[0].timeseries[key].confirmed != 0) {

                  var serie = data[0].timeseries[key].confirmed;
                  var arr1 = key.split('/');
                  var label= arr1[1];
                  if (arr1[0] == 4) {
                        var resultElement = [];
                        resultElement[0] = label;
                        resultElement[1] = serie;
                        resultElement[2] =data[0].timeseries[key].recovered;
                        resultElement[3] =data[0].timeseries[key].deaths;
                        series.push(serie);
                        labels.push(label);
                        totales.push(resultElement);
                  }
                }
              },
            );
            var allData = totales.reverse()

            var result = {
              series: [series],
              labels: labels,
              poblacion: 17500000,
              totales: allData,
              porcentaje: (allData[0][1] *100)/ 17500000,
              mortalidad:  (allData[0][3] *100)/ totales[0][1],
              recuperacion:  (allData[0][2] *100)/ totales[0][1]

            }
            this.setState({ guate: result })

          })

        fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=HN')
          .then(res => res.json())
          .then((data) => {
            var series = [];
            var labels = [];
            var totales = [];


            Object.keys(data[0].timeseries).map(function(key, item){
                if (data[0].timeseries[key].confirmed != 0) {
                  var serie = data[0].timeseries[key].confirmed;
                  var arr1 = key.split('/');
                  var label= arr1[1];
                  if (arr1[0] == 4) {
                    var resultElement = [];
                        resultElement[0] = label;
                        resultElement[1] = serie;
                        resultElement[2] =data[0].timeseries[key].recovered;
                        resultElement[3] =data[0].timeseries[key].deaths;
                      series.push(serie);
                      labels.push(label);
                        totales.push(resultElement);

                  }
                }
              },

            );
            var allData = totales.reverse()

            var result = {
              series: [series],
              labels: labels,
              totales: allData,
              poblacion: 9588000,
              porcentaje: (allData[0][1] *100)/ 17500000,
              mortalidad:  (allData[0][3] *100)/ totales[0][1],
              recuperacion:  (allData[0][2] *100)/ totales[0][1]




            }
            this.setState({ honduras: result })

          })

          fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=SV')
          .then(res => res.json())
          .then((data) => {
            var series = [];
            var labels = [];
            var totales = [];


            Object.keys(data[0].timeseries).map(function(key, item){
                if (data[0].timeseries[key].confirmed != 0) {
                  var serie = data[0].timeseries[key].confirmed;
                  var arr1 = key.split('/');
                  var label= arr1[1];
                  if (arr1[0] == 4) {
                    var resultElement = [];
                        resultElement[0] = label;
                        resultElement[1] = serie;
                        resultElement[2] =data[0].timeseries[key].recovered;
                        resultElement[3] =data[0].timeseries[key].deaths;
                  series.push(serie);
                  labels.push(label);
                        totales.push(resultElement);

                  }
                }
              },

            );
            var allData = totales.reverse()

            var result = {
              series: [series],
              labels: labels,
              totales: allData,
              poblacion: 6421000,
              porcentaje: (allData[0][1] *100)/ 17500000,
              mortalidad:  (allData[0][3] *100)/ totales[0][1],
              recuperacion:  (allData[0][2] *100)/ totales[0][1]




            }
            this.setState({ salvador: result })

          })

          fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=CR')
          .then(res => res.json())
          .then((data) => {
            var series = [];
            var labels = [];

            Object.keys(data[0].timeseries).map(function(key, item){
                if (data[0].timeseries[key].confirmed != 0) {
                  var serie = data[0].timeseries[key].confirmed;
                  var arr1 = key.split('/');
                  var label= arr1[1];
                  if (arr1[0] == 4) {
                  series.push(serie);
                  labels.push(label);
                  }
                }
              },
            );
            var result = {
              series: [series],
              labels: labels
            }
            this.setState({ costa: result })

          })


      fetch('https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/brief')
          .then(res => res.json())
          .then((data) => {
          this.setState({ resume: data })
          })
          .catch(console.log)
      }

render() {
    const classes = makeStyles(styles);
    const datos = this.state.resume;
    const guate = this.state.guate;
    const honduras = this.state.honduras;
    const salvador = this.state.salvador;
    const costa = this.state.costa;


  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
                <Icon>people</Icon>
              </CardIcon>
              <p style={{color: "black"}}>Total de casos</p>
              <h3 style={{color: "black"}}>
              <NumberFormat value={datos.confirmed} displayType={'text'} thousandSeparator={true} />

                
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                <Icon>people</Icon>
              </CardIcon>
              <p style={{color: "black"}}>Personas Recuperadas</p>
              <h3 style={{color: "black"}}>               
              <NumberFormat value={datos.recovered} displayType={'text'} thousandSeparator={true} />
</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader color="danger" stats icon>
              <CardIcon color="danger">
                <Icon>people</Icon>
              </CardIcon>
              <p style={{color: "black"}}>Personas fallecidas</p>
              <h3 style={{color: "black"}}>
              <NumberFormat value={datos.deaths} displayType={'text'} thousandSeparator={true} />
</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <Card chart>
            <CardHeader color="success">
              <ChartistGraph
                className="ct-chart"
                data={guate}
                type="Line"
                options={dailySalesChart.options}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Últimas Estadísticas en Guatemala</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} />
                </span>{" "}
                Estadísticas de Mes de Abril
              </p>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <Card chart>
            <CardHeader color="warning">
              <ChartistGraph
                className="ct-chart"
                data={honduras}
                type="Line"
                options={dailySalesChart.options}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Últimas Estadísticas en El Salvador</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} />
                </span>{" "}
                Estadísticas de Mes de Abril
              </p>
            </CardBody>
          </Card>
        </GridItem>
                <GridItem xs={12} sm={12} md={6}>
          <Card chart>
            <CardHeader color="primary">
              <ChartistGraph
                className="ct-chart"
                data={salvador}
                type="Line"
                options={dailySalesChart.options}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Últimas Estadísticas en Honduras</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} />
                </span>{" "}
                Estadísticas de Mes de Abril
              </p>
            </CardBody>
          </Card>
        </GridItem>
                <GridItem xs={12} sm={12} md={6}>
          <Card chart>
            <CardHeader color="danger">
              <ChartistGraph
                className="ct-chart"
                data={costa}
                type="Line"
                options={dailySalesChart.options}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Últimas Estadísticas en Costa Rica</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} />
                </span>{" "}
                Estadísticas de Mes de Abril
              </p>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
      {guate.totales ? 
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Resultados Guatemala</h4>
              <p className={classes.cardCategoryWhite}>
                Estadísticas mes de Abril
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="warning"
                tableHead={["Fecha", "Casos", "Recuperados", "Fallecidos"]}
                tableData={guate.totales}
              />
            </CardBody>
          </Card>
        </GridItem>
        : null } 

        {honduras.totales ? 
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Resultados Honduras</h4>
              <p className={classes.cardCategoryWhite}>
                Estadísticas mes de Abril
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="warning"
                tableHead={["Fecha", "Casos", "Recuperados", "Fallecidos"]}
                tableData={honduras.totales}
              />
            </CardBody>
          </Card>
        </GridItem>
        : null } 


        {salvador.totales ? 
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Resultados El Salvador</h4>
              <p className={classes.cardCategoryWhite}>
                Estadísticas mes de Abril
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="warning"
                tableHead={["Fecha", "Casos", "Recuperados", "Fallecidos"]}
                tableData={salvador.totales}
              />
            </CardBody>
          </Card>
        </GridItem>
        : null } 


        {guate.totales  && honduras.totales && salvador.totales ? 
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Tasa de Infección basado en población</h4>
              <p className={classes.cardCategoryWhite}>
              </p>
            </CardHeader>
            <CardBody>
             <table style={{textAlign: "center"}}>
             <thead>
              <tr>
                <th>Pais</th>
                <th>Casos confirmados</th>
                <th>Población</th>
                <th>Tasa actual de infección</th>

              </tr>
            </thead>
             <tr>
            <td>Guatemala</td>
            <td>{guate.totales[0][1]} </td>
            <td>{guate.poblacion}</td>
            <td>{guate.porcentaje.toFixed(5)}%</td>
             </tr>

            <tr>
            <td>Honduras</td>
            <td>{honduras.totales[0][1]} </td>
            <td>{honduras.poblacion}</td>
            <td>{honduras.porcentaje.toFixed(5)}%</td>
             </tr>


                         <tr>
            <td>El Salvador</td>
            <td>{salvador.totales[0][1]} </td>
            <td>{salvador.poblacion}</td>
            <td>{salvador.porcentaje.toFixed(5)}%</td>
             </tr>

             </table>
            </CardBody>
          </Card>
        </GridItem>
        : null } 



{guate.totales  && honduras.totales && salvador.totales ? 
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Tasa de Mortalidad</h4>
              <p className={classes.cardCategoryWhite}>
              </p>
            </CardHeader>
            <CardBody>
             <table style={{textAlign: "center"}}>
             <thead>
              <tr>
                <th>Pais</th>
                <th>Casos confirmados</th>
                <th>Muertes</th>
                <th>Tasa de mortalidad</th>

              </tr>
            </thead>
             <tr>
            <td>Guatemala</td>
            <td>{guate.totales[0][1]} </td>
            <td>{guate.totales[0][3]} </td>
            <td>{guate.mortalidad.toFixed(5)}%</td>
             </tr>

            <tr>
            <td>Honduras</td>
            <td>{honduras.totales[0][1]} </td>
            <td>{honduras.totales[0][3]} </td>
            <td>{honduras.mortalidad.toFixed(5)}%</td>
             </tr>


                         <tr>
            <td>El Salvador</td>
            <td>{salvador.totales[0][1]} </td>
            <td>{salvador.totales[0][3]} </td>
            <td>{salvador.mortalidad.toFixed(5)}%</td>
             </tr>

             </table>
            </CardBody>
          </Card>
        </GridItem>
        : null } 


{guate.totales  && honduras.totales && salvador.totales ? 
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Tasa de Recuperación</h4>
              <p className={classes.cardCategoryWhite}>
              </p>
            </CardHeader>
            <CardBody>
             <table style={{textAlign: "center"}}>
             <thead>
              <tr>
                <th>Pais</th>
                <th>Casos confirmados</th>
                <th>Recuperados</th>
                <th>Tasa de Recuperación</th>

              </tr>
            </thead>
             <tr>
            <td>Guatemala</td>
            <td>{guate.totales[0][1]} </td>
            <td>{guate.totales[0][2]} </td>
            <td>{guate.recuperacion.toFixed(5)}%</td>
             </tr>

            <tr>
            <td>Honduras</td>
            <td>{honduras.totales[0][1]} </td>
            <td>{honduras.totales[0][2]} </td>
            <td>{honduras.recuperacion.toFixed(5)}%</td>
             </tr>


                         <tr>
            <td>El Salvador</td>
            <td>{salvador.totales[0][1]} </td>
            <td>{salvador.totales[0][2]} </td>
            <td>{salvador.recuperacion.toFixed(5)}%</td>
             </tr>

             </table>
            </CardBody>
          </Card>
        </GridItem>
        : null } 

      </GridContainer>
    </div>
  );
}
}
export default Dashboard
